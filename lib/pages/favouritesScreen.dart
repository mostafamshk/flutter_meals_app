// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';

import '../Widgets/MealItem.dart';
import '../models/Meal.dart';

class FavouritesScreen extends StatelessWidget {
  final List<Meal> favouriteMeals;

  const FavouritesScreen({
    Key? key,
    required this.favouriteMeals,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return favouriteMeals.isEmpty
        ? Container(
            padding: EdgeInsets.all(20),
            child: Center(
              child: Text("No favourite meal added yet!"),
            ),
          )
        : ListView.builder(
            itemBuilder: (ctx, index) {
              return MealItem(meal: favouriteMeals.elementAt(index));
            },
            itemCount: favouriteMeals.length,
          );
  }
}
