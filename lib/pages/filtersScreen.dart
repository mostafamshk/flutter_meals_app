// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';

import 'package:mealsapp/Widgets/Drawer.dart';

import '../models/filters.dart';

class FiltersScreen extends StatelessWidget {
  static const route = '/filters';

  final Filters filters;
  final Function updateFilter;

  const FiltersScreen({
    Key? key,
    required this.filters,
    required this.updateFilter,
  }) : super(key: key);

  Widget buildFilterItem(String title, String subTitle, bool value) {
    return SwitchListTile(
        title: Text(title),
        subtitle: Text(subTitle),
        value: value,
        onChanged: (value) {
          updateFilter(title.toLowerCase(), value);
        });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Filters",
          style: Theme.of(context).textTheme.headline1,
        ),
      ),
      drawer: MyDrawer(),
      body: Container(
        width: double.infinity,
        padding: EdgeInsets.all(20),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Padding(
              padding: const EdgeInsets.only(bottom: 20.0),
              child: Text(
                "Adjust your filters!",
                style: Theme.of(context).textTheme.bodyText1,
              ),
            ),
            buildFilterItem(
              "Gluten-Free",
              "Only meals that are Gluten-freen",
              filters.gluten_free,
            ),
            Divider(),
            buildFilterItem(
              "Vegetarian",
              "Only Vegetarian Meals",
              filters.vegetarian,
            ),
            Divider(),
            buildFilterItem(
              "Vegan",
              "Only Vegan meals",
              filters.vegan,
            ),
            Divider(),
            buildFilterItem("Lactose-Free", "Only meals that are Lactose-freen",
                filters.lactose_free),
          ],
        ),
      ),
    );
  }
}
