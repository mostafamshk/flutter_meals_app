// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:flutter/material.dart';

import 'package:mealsapp/Widgets/MealItem.dart';
import 'package:mealsapp/constants/dummy_data.dart';
import 'package:mealsapp/models/filters.dart';

import '../models/Meal.dart';

class CategoryMealsScreen extends StatelessWidget {
  static const route = '/category-meals-screen';

  final Filters filters;

  const CategoryMealsScreen({
    Key? key,
    required this.filters,
  }) : super(key: key);

  bool shouldShowBasedOnFilters(Meal meal) {
    return !filters.filterIsActive() ||
        (filters.gluten_free && meal.isGlutenFree) ||
        (filters.vegetarian && meal.isVegetarian) ||
        (filters.vegan && meal.isVegan) ||
        (filters.lactose_free && meal.isLactoseFree);
  }

  @override
  Widget build(BuildContext context) {
    final routeArgs =
        ModalRoute.of(context)!.settings.arguments as Map<String, String>;
    final String categoryId = routeArgs['id']!;
    final String categoryTitle = routeArgs['title']!;
    final categoryMeals = DUMMY_MEALS
        .where((element) =>
            element.categories.contains(categoryId) &&
            shouldShowBasedOnFilters(element))
        .toList();

    return Scaffold(
        appBar: AppBar(
          title: Text(
            categoryTitle,
            style: Theme.of(context).textTheme.headline1,
          ),
        ),
        body: categoryMeals.isEmpty
            ? Container(
                alignment: Alignment.center,
                child: Center(
                  child: Text(
                    "No meals based on your prefered filters!",
                    textAlign: TextAlign.center,
                  ),
                ),
              )
            : ListView.builder(
                itemBuilder: (ctx, index) {
                  return MealItem(meal: categoryMeals.elementAt(index));
                },
                itemCount: categoryMeals.length,
              ));
  }
}
