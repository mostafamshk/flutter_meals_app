// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';

import 'package:mealsapp/Widgets/Drawer.dart';
import 'package:mealsapp/pages/categoriesScreen.dart';
import 'package:mealsapp/pages/favouritesScreen.dart';

import '../models/Meal.dart';

class TabsScreen extends StatefulWidget {
  static const route = '/';
  final List<Meal> favouriteMeals;
  

  const TabsScreen({
    Key? key,
    required this.favouriteMeals,
  }) : super(key: key);

  @override
  State<TabsScreen> createState() => _TabsScreenState();
}

class _TabsScreenState extends State<TabsScreen> {
  bool flag = true;
  List<Map<String, dynamic>> tabsScreens = [];

  @override
  void didChangeDependencies() {
    if (flag) {
      tabsScreens = [
        {'page': CategoriesScreen(), 'title': "Categories"},
        {'page': FavouritesScreen(favouriteMeals: widget.favouriteMeals), 'title': "Your Favourites"},
      ];
      flag = false;
    }

    super.didChangeDependencies();
  }

  int selectedTabIndex = 0;

  void selectTab(int index) {
    setState(() {
      selectedTabIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: MyDrawer(),
      appBar: AppBar(
        title: Text(tabsScreens[selectedTabIndex]['title']),
      ),
      body: tabsScreens[selectedTabIndex]['page'],
      bottomNavigationBar: BottomNavigationBar(
        onTap: (value) {
          selectTab(value);
        },
        selectedItemColor: Theme.of(context).colorScheme.secondary,
        backgroundColor: Theme.of(context).colorScheme.primary,
        unselectedItemColor: Colors.white,
        currentIndex: selectedTabIndex,
        // type: BottomNavigationBarType.shifting,
        items: [
          BottomNavigationBarItem(
              icon: Icon(Icons.category), label: "Categories"),
          BottomNavigationBarItem(icon: Icon(Icons.star), label: "Favourites"),
        ],
      ),
    );
  }
}
