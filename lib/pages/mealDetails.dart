// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';

import '../models/Meal.dart';

class MealDetails extends StatelessWidget {
  static const route = '/meal-details';

  final List<Meal> favouriteMeals;
  final Function updateFavouriteMeals;

  const MealDetails({
    Key? key,
    required this.favouriteMeals,
    required this.updateFavouriteMeals,
  }) : super(key: key);

  Widget buildBoxTitle(BuildContext context, String text) {
    return Center(
      child: Padding(
        padding: const EdgeInsets.only(bottom: 10.0, top: 20),
        child: Text(
          text,
          style: Theme.of(context).textTheme.bodyText1,
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    Meal meal = ModalRoute.of(context)!.settings.arguments as Meal;
    return Scaffold(
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          updateFavouriteMeals(meal);
        },
        child: Icon(
          favouriteMeals.any((element) => element.id == meal.id)
              ? Icons.star
              : Icons.star_border,
          color: Colors.pink,
          size: 30,
        ),
      ),
      appBar: AppBar(
        title: Text(
          "${meal.title}",
          style: Theme.of(context).textTheme.headline1,
        ),
      ),
      body: SafeArea(
          child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Container(
              width: double.infinity,
              height: 300,
              child: Image.asset(
                meal.imageUrl,
                fit: BoxFit.cover,
              ),
            ),
            buildBoxTitle(context, "Ingredients"),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20.0),
              child: Card(
                elevation: 10,
                child: Container(
                  padding: EdgeInsets.all(10),
                  height: 200,
                  child: ListView.builder(
                    itemBuilder: (context, index) {
                      return Container(
                        margin: EdgeInsets.symmetric(vertical: 5),
                        padding: EdgeInsets.all(10),
                        width: 200,
                        decoration: BoxDecoration(
                            color: Theme.of(context).colorScheme.secondary,
                            border: Border.all(color: Colors.grey, width: 1),
                            borderRadius: BorderRadius.circular(10)),
                        child: Text(
                          meal.ingredients[index],
                          style: Theme.of(context).textTheme.bodyText2,
                        ),
                      );
                    },
                    itemCount: meal.ingredients.length,
                  ),
                ),
              ),
            ),
            buildBoxTitle(context, "Steps"),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20.0),
              child: Card(
                elevation: 10,
                child: Container(
                  padding: EdgeInsets.all(10),
                  height: 200,
                  child: ListView.builder(
                    itemBuilder: (context, index) {
                      return Column(
                        children: [
                          ListTile(
                            leading: CircleAvatar(
                              backgroundColor:
                                  Theme.of(context).colorScheme.primary,
                              foregroundColor:
                                  Theme.of(context).colorScheme.onPrimary,
                              child: Text("#${index + 1}"),
                            ),
                            title: Text(
                              meal.steps[index],
                              style: Theme.of(context).textTheme.subtitle2,
                            ),
                          ),
                          Divider()
                        ],
                      );
                    },
                    itemCount: meal.steps.length,
                  ),
                ),
              ),
            ),
            SizedBox(
              height: 40,
            )
          ],
        ),
      )),
    );
  }
}
