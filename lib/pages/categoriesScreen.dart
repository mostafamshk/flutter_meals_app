import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/material.dart';
import 'package:mealsapp/Widgets/CategoryItem.dart';
import 'package:mealsapp/constants/dummy_data.dart';

class CategoriesScreen extends StatelessWidget {
  static const route = '/';

  const CategoriesScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return GridView(
      padding: EdgeInsets.all(15),
      gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
        maxCrossAxisExtent: (MediaQuery.of(context).size.width - 50) / 2,
        childAspectRatio: 3 / 2,
        crossAxisSpacing: 20,
        mainAxisSpacing: 20,
      ),
      children: DUMMY_CATEGORIES.map((e) => CategoryItem(category: e)).toList(),
    );
  }
}
