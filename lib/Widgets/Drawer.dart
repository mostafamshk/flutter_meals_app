import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';

import 'package:flutter/material.dart';
import 'package:mealsapp/pages/categoriesScreen.dart';
import 'package:mealsapp/pages/filtersScreen.dart';
import 'package:mealsapp/pages/tabsScreen.dart';

class MyDrawer extends StatelessWidget {
  const MyDrawer({super.key});

  Widget buildListItem(
      BuildContext context, IconData icon, String title, String route) {
    return Padding(
      padding: const EdgeInsets.only(left: 20.0),
      child: ListTile(
        leading: Icon(
          icon,
          // color: Colors.white,
        ),
        title: Text(title, style: Theme.of(context).textTheme.bodyText1!
            // .copyWith(color: Colors.white),
            ),
        onTap: () {
          Navigator.of(context).pushReplacementNamed(route);
        },
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Drawer(
      elevation: 5,
      child: Container(
        // color: Theme.of(context).colorScheme.primary,
        width: double.infinity,
        // padding: EdgeInsets.all(20),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              width: double.infinity,
              padding: EdgeInsets.only(top: 60, bottom: 30, left: 20),
              color: Theme.of(context).colorScheme.secondary,
              child: Text(
                "Meal App!",
                style: Theme.of(context).textTheme.bodyText1,
              ),
            ),
            buildListItem(context, Icons.restaurant, "Meals", TabsScreen.route),
            buildListItem(context, Icons.filter, "Filters", FiltersScreen.route)
          ],
        ),
      ),
    );
  }
}
