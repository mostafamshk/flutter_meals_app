class Human {
  String name;
  int age;
  int energy;
  DateTime bornTime;
  Human? father;
  Human(
      {required this.name,
      required this.age,
      required this.bornTime,
      required this.father,
      required this.energy});

  void sleep() {
    energy = 100;
  }

  void eat() {
    energy = energy + 50 > 100 ? 100 : energy + 50;
  }

  void run() {
    energy = energy - 40 > 0 ? energy - 40 : 0;
  }
}

void main(List<String> args) {
  Human mostafa = Human(
      name: "Mostafa",
      age: 17,
      bornTime: DateTime.now(),
      father: null,
      energy: 100);

  mostafa.run();
  print(mostafa.energy);
  ;
}
