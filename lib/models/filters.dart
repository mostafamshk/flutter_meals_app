// ignore_for_file: public_member_api_docs, sort_constructors_first
class Filters {
  bool gluten_free;
  bool vegetarian;
  bool vegan;
  bool lactose_free;
  Filters({
    required this.gluten_free,
    required this.vegetarian,
    required this.vegan,
    required this.lactose_free,
  });

  bool filterIsActive() {
    return gluten_free || vegetarian || vegan || lactose_free;
  }

  @override
  String toString() {
    return 'gluten-free : ${gluten_free}\n vegetarian : ${vegetarian}\n vegan : ${vegan}\n lactose_free : ${lactose_free}';
  }
}
