import 'package:flutter/material.dart';
import 'package:mealsapp/models/filters.dart';
import 'package:mealsapp/pages/favouritesScreen.dart';
import './pages/categoryMealsScreen.dart';
import './pages/notFoundScreen.dart';
import './pages/tabsScreen.dart';

import 'models/Meal.dart';
import 'pages/filtersScreen.dart';
import 'pages/mealDetails.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatefulWidget {
  const MyApp({super.key});

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  List<Meal> favouriteMeals = [];
  Filters filters = Filters(
      gluten_free: false, vegetarian: false, vegan: false, lactose_free: false);

  void updateFilter(String filter, bool value) {
    setState(() {
      if (filter == 'gluten-free') {
        filters.gluten_free = value;
      } else if (filter == 'vegetarian') {
        filters.vegetarian = value;
      } else if (filter == 'vegan') {
        filters.vegan = value;
      } else if (filter == 'lactose-free') {
        filters.lactose_free = value;
      }
    });
  }

  void updateFavouriteMeals(Meal meal) {
    setState(() {
      int index = favouriteMeals.indexWhere((element) => element.id == meal.id);
      if (index >= 0) {
        favouriteMeals.removeAt(index);
      } else {
        favouriteMeals.add(meal);
      }
    });
  }

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    // print(filters.toString());
    return MaterialApp(
      title: 'Flutter Demo',
      // onGenerateRoute: ,
      onUnknownRoute: (settings) {
        return MaterialPageRoute(
          builder: (context) {
            return NotFoundScreen();
          },
        );
      },
      theme: ThemeData(
          colorScheme: ColorScheme.fromSeed(
              seedColor: Colors.pink, secondary: Colors.amber),
          fontFamily: 'RobotoMono',
          textTheme: ThemeData.light().textTheme.copyWith(
              bodyText1: TextStyle(
                fontSize: 20,
                fontWeight: FontWeight.bold,
              ),
              bodyText2: TextStyle(fontSize: 16, fontWeight: FontWeight.normal),
              headline1: TextStyle(
                  fontSize: 25,
                  fontWeight: FontWeight.bold,
                  color: Colors.white),
              subtitle2:
                  TextStyle(fontSize: 14, fontWeight: FontWeight.normal))),
      // home: const CategoriesScreen(),
      routes: {
        TabsScreen.route: (ctx) => TabsScreen(
              favouriteMeals: favouriteMeals,
            ),
        CategoryMealsScreen.route: (ctx) =>
            CategoryMealsScreen(filters: filters),
        MealDetails.route: (ctx) => MealDetails(
              favouriteMeals: favouriteMeals,
              updateFavouriteMeals: updateFavouriteMeals,
            ),
        FiltersScreen.route: (ctx) => FiltersScreen(
              filters: filters,
              updateFilter: updateFilter,
            ),
      },
    );
  }
}
